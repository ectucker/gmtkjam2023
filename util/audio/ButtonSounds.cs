using Godot;
using System;

/// <summary>
/// A script for a node that plays sounds when a button is hovered on or pressed.
/// Sounds may be any kind of audio stream, collection, etc.
/// </summary>
public class ButtonSounds : Node
{
	private BaseButton _button;
	private AudioStreamProxy _hoverSound;
	private AudioStreamProxy _pressedSound;
	
	public override void _Ready()
	{
		base._Ready();

		_hoverSound = new AudioStreamProxy(GetNode("Hover"));
		_pressedSound = new AudioStreamProxy(GetNode("Press"));

		if (GetParent() is BaseButton button)
		{
			_button = button;
			button.Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hover));
			button.Connect(SignalNames.CONTROL_FOCUS_ENTERED, this, nameof(_Hover));
			button.Connect(SignalNames.BUTTON_PRESSED, this, nameof(_Pressed));
		}
	}

	private void _Hover()
	{
		if (!_button.Disabled)
		{
			if (!_hoverSound.Playing)
				_hoverSound.Play();
		}
	}

	private void _Pressed()
	{
		if (!_pressedSound.Playing)
			_pressedSound.Play();
	}
}
