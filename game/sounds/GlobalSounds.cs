using Godot;
using System;

public class GlobalSounds : Node
{
    public static AAudioStreamCollection AttackSounds;

    public static AAudioStreamCollection HealSounds;

    public static AAudioStreamCollection BuffSounds;

    public static AAudioStreamCollection DebuffSounds;

    public static AAudioStreamCollection StatusSounds;

    public override void _Ready()
    {
        AttackSounds = GetNode<AAudioStreamCollection>("AttackSound");
        HealSounds = GetNode<AAudioStreamCollection>("HealSound");
        BuffSounds = GetNode<AAudioStreamCollection>("BuffSound");
        DebuffSounds = GetNode<AAudioStreamCollection>("DebuffSound");
        StatusSounds = GetNode<AAudioStreamCollection>("StatusSound");
    }
}
