using Godot;
using System;

public class StartButton : Button
{
    bool pressed = false;
    
    public override void _Pressed()
    {
        base._Pressed();

        if (!pressed)
        {
            GetTree().ChangeScene("res://game/ui/combat_screen.tscn");
            pressed = true;
            GameStateHolder.Instance.CallDeferred(nameof(GameStateHolder._InitNotify));
        }
    }
}
