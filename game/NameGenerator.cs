using System;
using System.Collections.Generic;
using System.Text;

public static class NameGenerator {


    private static Random r = new Random();

    private static List<List<string>> StartEnd = new List<List<string>>(){
        new List<string>(){"XXX", "XXX"},
        new List<string>(){"XXX_", "_XXX"},
        new List<string>(){"xXx_", "_xXx"},
        new List<string>(){"!i!i!", "!i!i!"},
        new List<string>(){"|", "|"},
        new List<string>(){"~~~", "~~~"},
        new List<string>(){"#", "#"},
        new List<string>(){"[", "]"},
        new List<string>(){"[[", "]]"},
        new List<string>(){"(", ")"},
        new List<string>(){"|-", "-|"},
        new List<string>(){"-", "-"},
        new List<string>(){"~-", "-~"},
        new List<string>(){"%%%", "%%%"},
        new List<string>(){"$$$", "$$$"},
        new List<string>(){"==", "=="},
        new List<string>(){"~=~=", "=~=~"},
        new List<string>(){"OoO", "OoO"},
        new List<string>(){"0O0", "0O0"},
        new List<string>(){"xXxXxXx_", "_xXxXxXx"},
        new List<string>(){"$%&", "&%$"},
        new List<string>(){"___", "___"},
        new List<string>(){":", ":"},
        new List<string>(){"\\_", "_/"},
        new List<string>(){"/\\_", "_/\\"},
        
    };

    private static List<string> Prefixes = new List<string>(){
        "God",
        "God_",
        "Slayer",
        "Slayer_",
        "Dr.",
        "Dr",
        "Mega",
        "Mega_",
        "Lord",
        "Lord_",
        "Dude_",
        "Dude",
        "Mr.",
        "Mr",
        "King_",
        "King",
        "The",
        "The_",
        "The-",
        "th3",
        "7h3",
        "5ir",
        "Sir",
        "Sir_",
        "5ir",
        "TheOneAndOnly_"
    };

    private static List<string> Names = new List<string>(){
        "Egg",
        "Eggington",
        "Dragon",
        "Demon",
        "Gamer",
        "Dark_G0D",
        "$%^^##",
        "Zer0",
        "Guy",
        "Noob",
        "Winter",
        "Gam30v3r",
        "Angel",
        "Wiz",
        "SNAP",
        "DOOM",
        "D00M",
        "Salty",
        "DogGamer",
        "DawgGam3r",
        "GGEZ",
        "ThisGameSux",
        "GetGud",
        "888",
        "B@dGuy",
        "HERO",
        "ImMaxLevelOnMyMain",
        "0M3GA",
        "DarkWolf",
        "gladiator",
        "LERRROOOYJENKINS",
        "NamesAreDumb",
        "Unkillable",
        "hacKEr"
    };



    public static string GenerateName(){
        StringBuilder sb = new StringBuilder();
        List<string> prePost = StartEnd[r.Next(0, StartEnd.Count)];
        sb.Append(prePost[0]);
        sb.Append(Prefixes[r.Next(0, Prefixes.Count)]);        
        sb.Append(Names[r.Next(0, Names.Count)]);
        sb.Append(prePost[1]);


        return sb.ToString();
    }


}