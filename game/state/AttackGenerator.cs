using System.Collections.Generic;
using System;
using System.Text;

public static class AttackGenerator {

    private static Random r = new Random();

    private static int low = 1;
    private static int high = 4;

    private static List<Func<int, IActionEffect>> AllyEffects = new List<Func<int, IActionEffect>>(){
        (int level) => { return new AOEHealEffect(r.Next(low, 2) * level);},        
        (int level) => { return new AOEHealOverTimeEffect(level, Math.Max(1,level / 3 ));},
        (int level) => { return new HealEffect(r.Next(low, high) * level * r.Next(1,4));},
        (int level) => { return new HealOverTimeEffect(r.Next(low, 2) * level, Math.Max(1,level / 3 ));},         
        (int level) => { return new DamageUpEffect(r.Next(1, 3) * Math.Max(1,level / 3 ));}, 
        (int level) => { return new DefenseUpEffect(r.Next(1, 3) * Math.Max(1,level / 3 ));}     
    };

    private static List<Func<int, IActionEffect>> EnemyEffects = new List<Func<int, IActionEffect>>(){
        (int level) => { return new AOEDamageEffect(r.Next(low, high) * level);},
        (int level) => { return new AOEDamageOverTimeEffect(level, Math.Max(1,level / 3 ));},
        (int level) => { return new AOEDamageEffect(r.Next(low, high) * level);},
        (int level) => { return new DamageActionEffect(r.Next(low, high) * level * r.Next(2,4));},
        (int level) => { return new DamageOverTimeEffect(r.Next(low, high) * level, Math.Max(1,level / 3 ));},
        (int level) => { return new DamageDownEffect(r.Next(1, 3) * Math.Max(1,level / 3 ));},
        (int level) => { return new DefenseDownEffect(r.Next(1, 3) * Math.Max(1,level / 3 ));}
    };

    private static List<string> Prefixes = new List<string>(){
        "Super",
        "Mega",
        "Turbo",
        "Insane",
        "OP",
        "Absurd",
        "Ultra",
        "Blazing",
        "Searing",
        "Deadly",
        "Otherworldy",
        "Incredible",
        "Powerful",
        "Threatening",
        "Unreal"
    };

    private static List<string> PostfixesBad = new List<string>(){
        "Of Death",
        "Of Power",
        "Of Devestation",
        "Of Evil",
        "Of Holy Might",
        "Of Destiny",
        "Of Destruction",
        "Of Hate",
        "Of Chaos"
    };

    private static List<string> PostfixesGood = new List<string>(){
        "Of Help",
        "Of Power",
        "Of Holy Might",
        "Of Destiny",
        "Of Destruction",
        "Of Guidance",
        "Of Blessings",
        "Of Kindness"
    };

    private static List<string> AttackMiddle = new List<string>(){
        "Attack",
        "Strike",
        "Blow",
        "Swing",
        "Shot",
        "Stabbing"
    };

    private static List<string> BuffDebuffMiddle = new List<string>(){
        "Touch",
        "Words",
        "Voice",
        "Thoughts",
        "Spell",
        "Ritual",
        "Potion"
    };

    public static CombatAction GenerateAction(int level){
        
       int origLevel = level;
        
        List<IActionEffect> effects = new List<IActionEffect>(); 
        Func<Entity, Entity, bool> validTarget = null;


        if(r.Next(1,101) > 25){
            //make damage/debuff
            while(level > 1){
                effects.Add(EnemyEffects[r.Next(1, EnemyEffects.Count)](level));
                level -= r.Next(5, 10);
                if(effects.Count > 3) level -= 100;
                validTarget = TargetPredicate.OnlyOpposingTeam;
            }

        }else{
            //make heal/make buff
            while(level > 1){
                effects.Add(AllyEffects[r.Next(1, AllyEffects.Count)](level));
                level -= r.Next(5, 10);
                if(effects.Count > 3) level -= 100;
                validTarget = TargetPredicate.OnlySameTeam;
            }
        }
        ActionClass actionClass = effects[0].actionClass; //dispalying what they plan to do
        string name = GenName(effects);


        CombatAction action = new CombatAction(name, actionClass, origLevel, effects, validTarget);

        return action;
    }

    private static string GenName(List<IActionEffect> effects){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < r.Next(0, 2); i++){
            sb.Append(Prefixes[r.Next(0, Prefixes.Count)]);
            sb.Append(" ");
        }

        if(effects[0].actionClass == ActionClass.ATTACK){
            sb.Append(AttackMiddle[r.Next(0,AttackMiddle.Count)]);
            sb.Append(" ");
            sb.Append(PostfixesBad[r.Next(0,PostfixesBad.Count)]);
        }else if(effects[0].actionClass == ActionClass.HEAL){
            sb.Append(BuffDebuffMiddle[r.Next(0, BuffDebuffMiddle.Count)]);
            sb.Append(" ");
            sb.Append(PostfixesGood[r.Next(0,PostfixesGood.Count)]);
        }else if(effects[0].actionClass == ActionClass.BUFF){
            sb.Append(BuffDebuffMiddle[r.Next(0, BuffDebuffMiddle.Count)]);
            sb.Append(" ");
            sb.Append(PostfixesGood[r.Next(0,PostfixesGood.Count)]);
        }else{
            sb.Append(BuffDebuffMiddle[r.Next(0, BuffDebuffMiddle.Count)]);
            sb.Append(" ");
            sb.Append(PostfixesBad[r.Next(0,PostfixesBad.Count)]);
        }


        return sb.ToString();
    }


}