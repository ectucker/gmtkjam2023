public enum GameMode
{
    MAIN_MENU,
    COMBAT_START,
    SELECT_ATTACKS,
    EXECUTE_ATTACKS,
    LOOT,
    FINISHED
}