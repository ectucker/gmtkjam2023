using System.Collections.Generic;
using Godot;

public class Entity
{
    public Team Team { get; private set; }
    public string Name { get; private set; }

    public int damageUp {get; set;}

    public int damageDown {get; set;}

    public int defenseUp {get; set;}

    public int defenseDown {get; set;}

    public int Level
    {
        get
        { 
            if (Actions.Count <= 0)
                return 1;
            
            float total = 0;
            foreach (var action in Actions)
                total += action.Level;
            return Mathf.RoundToInt(total / Actions.Count);
        }
    }

    public int MaxHealth { get; private set; }

    public int Health { get; private set; }

    public List<CombatAction> Actions { get; private set; }

    public QueuedAction QueuedAction { get; set; }

    
    private List<AStatusEffect> _statuses = new List<AStatusEffect>();
    public IEnumerable<AStatusEffect> Statuses => _statuses;

    public Entity(Team team, string name, int maxHealth, List<CombatAction> actions)
    {
        this.Team = team;
        this.Name = name;
        this.MaxHealth = maxHealth;
        this.Health = maxHealth;
        this.Actions = actions;
    }

    public void ResetStatus()
    {
        Health = MaxHealth;
        defenseUp = 0;
        defenseDown = 0;
        damageUp = 0;
        damageDown = 0;
        _statuses.Clear();
    }

    public void Damage(int amount)
    {
        int value = (int)((float)amount  *  ((Mathf.Sqrt(damageUp) * 0.08)  - (Mathf.Sqrt(damageDown) * (-0.08)) + 1));
        Health = Mathf.Clamp(Health - value , 0, MaxHealth);
        if (Health <= 0)
        {
            switch (Team)
            {
                case Team.PLAYER:
                    GameState.MarkEvent(InterestingEvents.ENEMY_GOT_KILL);
                    break;
                case Team.ENEMY:
                    GameState.MarkEvent(InterestingEvents.PLAYER_GOT_KILL);
                    break;
            }
        }
    }

    public void Heal(int amount){
        if (Health > 0)
        {
            Health = Mathf.Clamp(Health + amount, 0, MaxHealth);
        }
    }

    public int DamageModifier(int baseDamage){
        int modDamage = (int)((float)baseDamage  *  (int)((Mathf.Sqrt(damageUp) * 0.08)  - (Mathf.Sqrt(damageDown) * (-0.08)) + 1));
        return modDamage;
    }

    public void AddStatus(AStatusEffect effect)
    {
        effect.Apply();
        _statuses.Add(effect);
    }

    public void RemoveStatus(AStatusEffect effect)
    {
        effect.Remove();
        _statuses.Remove(effect);
    }

    public void RecalcHealthSkeleton()
    {
        int level = Level;
        MaxHealth = 20 + 14 * level;
        Health = MaxHealth;
    }
}
