using System.Collections.Generic;
using System;

public static class EncounterList
{
    private static int _index = 0;

    public static Encounter repeatedEncounter = null;

    private static Random r = new Random();
    public static List<Encounter> Encounters = new List<Encounter>()
    {
        // Tutorial encounter
        new Encounter(
           new Entity(Team.ENEMY, "XXX_CoolDude", 20, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(5) }),
            new Entity(Team.ENEMY, "Tilda2", 20, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(5) }),
            new Entity(Team.ENEMY, "!@#&(*$(*))", 20, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(5) })
     ),
        RandomEncounter(6),
        RandomEncounter(10),
        RandomEncounter(15),
        RandomEncounter(20),
        RandomEncounter(25),
        RandomEncounter(30),
        MakeBossEncounter()
    };

    public static Encounter MakeBossEncounter()
    {
        return new Encounter(
            new Entity(Team.ENEMY, "Groxidius, Lord of Bones (and flowers)", 1500, new List<CombatAction> { PredefActionsFactory.BossAttack1(40), PredefActionsFactory.BossAttack2(40), PredefActionsFactory.BossAttack3(40), PredefActionsFactory.BossAttack4(40) }),
            new Entity(Team.ENEMY, "Left Skeleton", 500, new List<CombatAction> { PredefActionsFactory.SkelleAttack1(40), PredefActionsFactory.SkelleAttack2(40), PredefActionsFactory.SkelleAttack1(40) }),
            new Entity(Team.ENEMY, "Right Skeleton", 500, new List<CombatAction> { PredefActionsFactory.SkelleAttack2(40),  PredefActionsFactory.SkelleAttack1(40), PredefActionsFactory.SkelleAttack1(40)})
        );
    }

    public static bool HasNextEncounter()
    {
        return repeatedEncounter != null || _index < Encounters.Count;
    }

    public static Encounter NextEncounter()
    {
        if (_index >= Encounters.Count)
            return MakeBossEncounter();
        
        var encounter = Encounters[_index];
        repeatedEncounter = encounter;
        _index += 1;
        return encounter;
    }

    public static Encounter RandomEncounter(int level)
    {
        
        return new Encounter(RandomEnemy(level+r.Next(-2,3)), RandomEnemy(level+r.Next(-2,3)), RandomEnemy(level+r.Next(-2,3)));
    }

    public static Entity RandomEnemy(int level)
    {
        return new Entity(
            Team.ENEMY,
            NameGenerator.GenerateName(),
            level * 8,
            new List<CombatAction>() {
                AttackGenerator.GenerateAction(level),
                AttackGenerator.GenerateAction(level),
                AttackGenerator.GenerateAction(level),
            });
    }
}