using Godot;
using System;

public class GameStateHolder : Node
{
    public static GameStateHolder Instance { get; private set; }

    [Signal]
    public delegate void StateChanged();

    public override void _Ready()
    {
        base._Ready();

        Instance = this;

        CallDeferred(nameof(_InitNotify));
    }

    public void _InitNotify()
    {
        NotifyChanged();
    }

    public static void NotifyChanged()
    {
        Instance.EmitSignal(nameof(StateChanged));
    }
}
