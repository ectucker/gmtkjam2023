using System;

public static class TargetPredicate
{
    public static Func<Entity, Entity, bool> OnlyOpposingTeam = (target, source) => {
        return target.Health > 0 && target.Team != source.Team;
    };

    public static Func<Entity, Entity, bool> OnlySameTeam = (target, source) => {
        return target.Health > 0 && target.Team == source.Team;
    };
}