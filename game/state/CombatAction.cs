using System.Collections.Generic;
using System;

public class CombatAction
{
    public string Name { get; private set; }

    public int Level { get; private set; }

    public ActionClass ActionClass { get; private set; }

    public List<IActionEffect> Effects { get; private set; }

    public Func<Entity, Entity, bool> IsVaidTarget { get; private set; }

    public CombatAction(string name, ActionClass actionClass, int level, List<IActionEffect> effects, Func<Entity, Entity, bool> isValidTarget)
    {
        this.Name = name;
        this.ActionClass = actionClass;
        this.Level = level;
        this.IsVaidTarget = isValidTarget;

        List<IActionEffect> effectsCopy = new List<IActionEffect>(effects);
        bool foundCombo = false;
        do {
            foundCombo = false;
            IActionEffect foundEffect1 = null;
            IActionEffect foundEffect2 = null;
            foreach (var effect1 in effectsCopy)
            {
                foreach (var effect2 in effectsCopy)
                {
                    if (effect1 != effect2 && effect1.CanCombine(effect2) && !foundCombo)
                    {
                        foundEffect1 = effect1;
                        foundEffect2 = effect2;
                        foundCombo = true;
                    }
                }
            }

            if (foundCombo)
            {
                effectsCopy.Remove(foundEffect1);
                effectsCopy.Remove(foundEffect2);
                var combo = foundEffect1.Combine(foundEffect2);
                if (combo != null)
                {
                    effectsCopy.Add(combo);
                }
            }
        } while (foundCombo);

        Effects = effectsCopy;
    }

    public bool RequiresTarget()
    {
        foreach (var effect in Effects)
        {
            if (effect.RequiresTarget())
                return true;
        }
        return false;
    }
}
