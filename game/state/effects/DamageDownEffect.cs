public class DamageDownEffect : IActionEffect
{
 
    private int _time;

    public int time {get {return time; } set {_time = value; }}
    public override ActionClass actionClass { get;  set; } = ActionClass.DEBUFF;
    public DamageDownEffect(int time){
        this._time = time;   
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";

        target.AddStatus(new DamageDownStatus(target, _time, 0));
        return $"applying damage down modifier for {_time} turns.";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Applies a damage debuff for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DamageDownEffect effect)
        {
            return new DamageDownEffect(effect._time + this._time);
        }
        return null;
    }
}
