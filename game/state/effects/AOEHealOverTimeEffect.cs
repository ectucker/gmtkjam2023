using Godot;

public class AOEHealOverTimeEffect : IActionEffect
{

    private int _heal;
    private int _time;
    public override ActionClass actionClass { get;  set; } = ActionClass.HEAL;
    public int time {get {return _time;} set {_time = value;}}

    public AOEHealOverTimeEffect(int heal, int time){
        this._heal = heal;
        this._time = time;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            GameState.MarkEvent(InterestingEvents.PLAYER_USED_HEALING);
        }
        else if (user.Team == Team.ENEMY)
        {
            GameState.MarkEvent(InterestingEvents.ENEMY_USED_HEALING);
        }

        if (user.Team == Team.ENEMY)
        {
            foreach (var enemy in GameState.Enemies)
                enemy.AddStatus(new HealOverTimeStatus(enemy, _time, _heal));
            return $"healing {_heal} points of health over time for {_time} to the players";
        }
        else if (user.Team == Team.PLAYER)
        {
            foreach (var player in GameState.Players)
                player.AddStatus(new HealOverTimeStatus(player, _time, _heal));
            return $"healing {_heal} points of health over time for {_time} to the skeletons";
        }
        return "";
    }

    public override bool RequiresTarget()
    {
        return false;
    }

    public override string ToString()
    {
        return $"Heals for {_heal} points of health for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is AOEHealOverTimeEffect effect)
        {
            return new AOEHealOverTimeEffect(effect._heal + this._heal, Mathf.Max(effect._time, this._time));
        }
        return null;
    }
}
