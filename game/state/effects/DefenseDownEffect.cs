public class DefenseDownEffect : IActionEffect
{
 
    private int _time;

    public int time {get {return time; } set {_time = value; }}
    public override ActionClass actionClass { get;  set; } = ActionClass.DEBUFF;
    public DefenseDownEffect(int time){
        this._time = time;   
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";
        target.AddStatus(new DefenseDownStatus(target, _time, 0));
        return $"applying defense down for {_time} turns";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Applies a defense debuff for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DefenseDownEffect effect)
        {
            return new DefenseDownEffect(effect._time + this._time);
        }
        return null;
    }
}
