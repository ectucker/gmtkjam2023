public class AOEHealEffect : IActionEffect
{
    private int _heal;
    public override ActionClass actionClass { get;  set; } = ActionClass.HEAL;
    public AOEHealEffect(int amount){
        this._heal = amount;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            GameState.MarkEvent(InterestingEvents.PLAYER_USED_HEALING);
        }
        else if (user.Team == Team.ENEMY)
        {
            GameState.MarkEvent(InterestingEvents.ENEMY_USED_HEALING);
        }

        if (user.Team == Team.PLAYER)
        {
            foreach (var player in GameState.Players)
                player.Heal(_heal);
            return $"healing the skeletons by {_heal}";
        }
        else if (user.Team == Team.ENEMY)
        {
            foreach (var enemy in GameState.Enemies)
                enemy.Heal(_heal); 
            return $"healing the players by {_heal}";
        }
        return "";
    }

    public override bool RequiresTarget()
    {
        return false;
    }

        public override string ToString()
    {
        return $"Heals your team for {_heal} points of health.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is AOEHealEffect effect)
        {
            return new AOEHealEffect(effect._heal + this._heal);
        }
        return null;
    }
}