using Godot;

public class HealOverTimeEffect : IActionEffect
{

    private int _heal;
    private int _time;

    public int time {get {return _time;} set {_time = value;}}
    public override ActionClass actionClass { get;  set; } = ActionClass.HEAL;
    public HealOverTimeEffect(int heal, int time){
        this._heal = heal;
        this._time = time;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            GameState.MarkEvent(InterestingEvents.PLAYER_USED_HEALING);
        }
        else if (user.Team == Team.ENEMY)
        {
            GameState.MarkEvent(InterestingEvents.ENEMY_USED_HEALING);
        }

        if (target == null)
            return "";
        target.AddStatus(new HealOverTimeStatus(target, _time, _heal));
        return $"healing {target.Name} by {_heal} for {_time} turns";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Heals {_heal} points of health for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is HealOverTimeEffect effect)
        {
            return new HealOverTimeEffect(effect._heal + this._heal, Mathf.Max(effect._time, this._time));
        }
        return null;
    }
}
