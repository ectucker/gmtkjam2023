public class HealEffect : IActionEffect
{
    private int _heal;

    public HealEffect(int amount){
        this._heal = amount;
    }
    public override ActionClass actionClass { get;  set; } = ActionClass.HEAL;
    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            GameState.MarkEvent(InterestingEvents.PLAYER_USED_HEALING);
        }
        else if (user.Team == Team.ENEMY)
        {
            GameState.MarkEvent(InterestingEvents.ENEMY_USED_HEALING);
        }

        if (target == null)
            return "";
       target.Heal(_heal);
       return $"healing {target.Name} by {_heal}";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Heals target for {_heal} points of health.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is HealEffect effect)
        {
            return new HealEffect(effect._heal + this._heal);
        }
        return null;
    }
}