using Godot;

public class AOEDamageOverTimeEffect : IActionEffect
{

    private int _damage;
    private int _time;
    public override ActionClass actionClass { get; set; } = ActionClass.ATTACK;
    public int time {get {return _time;} set {_time = value;}}

    public AOEDamageOverTimeEffect(int damage, int time){
        this._damage = damage;
        this._time = time;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            foreach (var enemy in GameState.Enemies)
                enemy.AddStatus(new DamageOverTimeStatus(enemy, _time, user.DamageModifier(_damage)));
            return $"applying {user.DamageModifier(_damage)} damage over time for {_time} to the players";
        }
        else if (user.Team == Team.ENEMY)
        {
            foreach (var player in GameState.Players)
                player.AddStatus(new DamageOverTimeStatus(player, _time, user.DamageModifier(_damage)));
            return $"applying {user.DamageModifier(_damage)} damage over time for {_time} to the skeletons";
        }
        return "";
    }

    public override bool RequiresTarget()
    {
        return false;
    }

    public override string ToString()
    {
        return $"Deals {_damage} damage to all enemies for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is AOEDamageOverTimeEffect effect)
        {
            return new AOEDamageOverTimeEffect(effect._damage + this._damage, Mathf.Max(effect._time, this._time));
        }
        return null;
    }
}
