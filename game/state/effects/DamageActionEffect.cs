public class DamageActionEffect : IActionEffect
{
    private int _damage;
    public override ActionClass actionClass { get;  set; } = ActionClass.ATTACK;
    public DamageActionEffect(int damage)
    {
        this._damage = damage;
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";
            
        target.Damage(user.DamageModifier(_damage));
        return $"dealing {user.DamageModifier(_damage)} damage to {target.Name}";
    }

    public override string ToString()
    {
        return $"Deals {_damage} Damage.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DamageActionEffect effect)
        {
            return new DamageActionEffect(effect._damage + this._damage);
        }
        return null;
    }
}