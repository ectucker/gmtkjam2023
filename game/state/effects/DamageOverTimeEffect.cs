using Godot;

public class DamageOverTimeEffect : IActionEffect
{
    private int _damage;
    private int _time;
    public override ActionClass actionClass { get;  set; } = ActionClass.ATTACK;
    public int time {get {return _time;} set {_time = value;}}

    public DamageOverTimeEffect(int damage, int time){
        this._damage = damage;
        this._time = time;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";
        
        target.AddStatus(new DamageOverTimeStatus(target, _time, user.DamageModifier(_damage)));
        return $"applying {user.DamageModifier(_damage)} damage over time for {_time} to {target.Name}";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Deals {_damage} damage for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DamageOverTimeEffect effect)
        {
            return new DamageOverTimeEffect(effect._damage + this._damage, Mathf.Max(effect._time, this._time));
        }
        return null;
    }
}
