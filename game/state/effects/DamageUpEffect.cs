public class DamageUpEffect : IActionEffect
{
 
    private int _time;

    public int time {get {return time; } set {_time = value; }}
    public override ActionClass actionClass { get;  set; } = ActionClass.BUFF;
    public DamageUpEffect(int time){
        this._time = time;   
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";
        target.AddStatus(new DamageUpStatus(target, _time, 0));
        return $"applying a damage buff for {_time} turns";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Applies a damage buff for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DamageUpEffect effect)
        {
            return new DamageUpEffect(effect._time + this._time);
        }
        return null;
    }
}
