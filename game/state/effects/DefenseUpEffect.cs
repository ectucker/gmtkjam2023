public class DefenseUpEffect : IActionEffect
{
 
    private int _time;

    public int time {get {return time; } set {_time = value; }}
    public override ActionClass actionClass { get;  set; } = ActionClass.BUFF;
    public DefenseUpEffect(int time){
        this._time = time;   
    }

    public override string Apply(Entity target, Entity user)
    {
        if (target == null)
            return "";
        target.AddStatus(new DefenseUpStatus(target, _time, 0));
        return $"applying defense down for {_time} turns";
    }

    public override bool RequiresTarget()
    {
        return true;
    }

    public override string ToString()
    {
        return $"Applies a defense buff for {_time} turns.";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is DefenseUpEffect effect)
        {
            return new DefenseUpEffect(effect._time + this._time);
        }
        return null;
    }
}
