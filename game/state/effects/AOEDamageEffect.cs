public class AOEDamageEffect : IActionEffect
{
    private int _damage;

    public override ActionClass actionClass { get; set; } = ActionClass.ATTACK;

    public AOEDamageEffect(int damage)
    {
        this._damage = damage;
    }

    public override bool RequiresTarget()
    {
        return false;
    }

    public override string Apply(Entity target, Entity user)
    {
        if (user.Team == Team.PLAYER)
        {
            foreach (var enemy in GameState.Enemies)
                enemy.Damage(user.DamageModifier(_damage));
            return $"dealing {_damage} to the players";
        }
        else if (user.Team == Team.ENEMY)
        {
            foreach (var player in GameState.Players)
                player.Damage(user.DamageModifier(_damage));  
            return $"dealing {_damage} to the skeletons";
        }
        return "";
    }

    public override string ToString()
    {
        return $"Deals {_damage} damage to all enemies";
    }

    public override IActionEffect Combine(IActionEffect other)
    {
        if (other is AOEDamageEffect effect)
        {
            return new AOEDamageEffect(effect._damage + this._damage);
        }
        return null;
    }
}