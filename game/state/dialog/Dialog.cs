using System.Collections.Generic;
using System;

public class Dialog
{
    public int Importance { get; private set; }

    public Func<bool> Condition { get; private set; }

    public List<Line> Lines { get; private set; }

    public int CurrentIndex { get; set; } = 0;

    public Dialog(int importance, Func<bool> condition, List<Line> lines)
    {
        Importance = importance;
        Condition = condition;
        Lines = lines;
    }

    public Line CurrentLine => Lines[CurrentIndex];
}
