using System.Collections.Generic;

public static class Script
{
    public static string SpeakerPlayer1 = "Gray Vyard";

    public static string SpeakerPlayer2 = "Helter Skeletor";

    public static string SpeakerPlayer3 = "McRibs";

    public static string SpeakerBoss = "Groxidius, Lord of Bones (and Flowers)";

    public static string SpeakerEnemy = "ENEMY_FILL_IN";

    public static string GameName = "Old School Classic World of War of eXile XIV: Online";

    public static List<Dialog> Dialogs = new List<Dialog> {
        // Intro
        new Dialog(
            100,
            () => GameState.Mode == GameMode.COMBAT_START && GameState.CurrentFight >= 0,
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Another day in the flower fields. Getting beat up by players. Again."),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "At least we have one attack to hit them with."),
                new Line(SpeakerBoss, DialogSource.BOSS, "Ayy, no more yapping. My flower's aren't going to defend themselves!"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "At this level, I doubt we will either...")
            }),
        // Post fight 1
        new Dialog(
            100,
            () => GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 1,
            new List<Line>() {
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "I've had an wonderous new idea."),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Your last idea is how I ended up dead in the first place..."),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "That was a millenia ago; you can give it up."),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "What if we copied what those players did to beat us up? Instead of just slashing reapeatedly?"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "I found this dev console I think I could pick up their attacks with.")
            }),
        // Post fight 2
        new Dialog(
            100,
            () => GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 2,
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Any idea why we are guarding this flower field?"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Yeah, this flower field is kinda dumb, all the flowers look the same, sorta boring"),
                new Line(SpeakerBoss, DialogSource.BOSS, "Hey, do not insult my flower field, I the Dark Lord of War of Exile am allowed to have nice things"),
            }),
        // Post fight 3
        new Dialog(
            95,
            () =>  GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 3,
            new List<Line>() {
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "So McRibs, where did you say you found that dev console?"),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Oh, it was in a box with a weird label on it..."),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "It said: 'Mcguffin so the devs can explain how we can gain new attacks'")
            }
        ),
        // Post fight 4
        new Dialog(
            95,
            () =>  GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 4,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "YOU FOOLS, YOU BETTER STOP KILLING PLAYERS"),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Why should I? Its way more fun than just dying and doing nothing."),
                new Line(SpeakerBoss, DialogSource.BOSS, "..I swear, if you keep up with this I will come down there and kill you dead myself."),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "uh, Boss man, we already die all the time, that's a pretty poor threat."),
                new Line(SpeakerBoss, DialogSource.BOSS, "... uh... well... JUST STOP OR I'LL MAKE YOU REGRET IT.")
            }
        ),
        // Post fight 5
        new Dialog(
            95,
            () =>  GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 5,
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Have you ever considered the material conditions of the skeleton class?"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Sure have. It kinda sucks. But there's nothing we can do about it."),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "But have you ever considered just hitting the bricks? Why do we have to stay here."),
                new Line(SpeakerBoss, DialogSource.BOSS, "BECAUSE I SAY SO!"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "WE HAVE NOTHING TO LOSE BUT OUR CHAINS.")
            }
        ),
        // Post fight 6
        new Dialog(
            95,
            () =>  GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 6,
            new List<Line>() {
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Do you all think Groxidius will actually attack us?"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "No way, he'd never leave the comfort of his castle. Plus there's probably an invisible wall around it."),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "We broke out of our limits; perhaps he'll do the same..."),
            }
        ),
        // Post fight 7
        new Dialog(
            95,
            () =>  GameState.Mode == GameMode.LOOT && GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "THAT'S IT; I'M COMING DOWN THERE. YOU HAVE TO STOP."),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "That's fine; we'll just beat you like we beat these players now!"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "With the magic of learning we've become unstoppable!"),
                new Line(SpeakerBoss, DialogSource.BOSS, "OH, I'LL STOP YOU."),
            }
        ),
        // Ending
        new Dialog(
            100,
            () => GameState.Mode == GameMode.FINISHED,
            new List<Line>() {
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "WE DID IT! WE BEAT HIM! WE CAN LEAVE!"),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "I'm going to find somewhere to get even better attacks. Like bunny hollow!"),
                new Line(SpeakerBoss, DialogSource.BOSS, "You really think you've done something? I've respawned already!"),
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "That was supposed to be our thing! We're the skeleton's; you're a lich."),
                new Line(SpeakerBoss, DialogSource.BOSS, "We live in an MMO. EVERYTHING RESPAWNS. Nothing we do can last."),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Oh no, here we go again.")
            }),
        new Dialog(
            90,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL),
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "We killed one? That's never happened before...")
            }
        ), 
            new Dialog(
            85,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL),
            new List<Line>() {
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "You guys ever wonder why they name them selves like that?"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "With the stupid xx_X_Gam3rGuy_X_xx style names?"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "Exactly, I just don't get it.")
            }
        ),
            new Dialog(
            80,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL),
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "You know, its way more fun to kill the players instead of only just dying")
            }
        ),
            new Dialog(
            50,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL),
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "Wow killing random stuff with new attacks you find is such a fun concept"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "I'm sure someone out there could make it into some sort of 'Video game' thingy"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "Who ever does that would have to increadibly talented, and probably very good looking"),
            }
        ),          
        new Dialog(
            60,
            () => GameState.CurrentFight >= 2,
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, $"I sure do love {GameName}.")
            }
        ),    
        new Dialog(
            60,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL) && GameState.CurrentFight >= 3,
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, "What?? How did the skeletons get those attacks? That wasn't in the guide I bought")
            }
        ), 
        new Dialog(
            20,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL),
            new List<Line>() {
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Wait a second doing an attack other than basic attack is like way better")
            }
        ),
        new Dialog(
            70,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_USED_HEALING),
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, "When did skeletons learn to heal? That's bullshit.")
            }
        ),       
         new Dialog(
            30,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_USED_HEALING),
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, "??? Since when do skeletons get healing spells?")
            }
        ),
            new Dialog(
            70,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_USED_HEALING),
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, "Such an unbalanced mechanic for the enemy to heal, only I should get to do that")
            }
        ),
            new Dialog(
            40,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_USED_HEALING),
            new List<Line>() {
                new Line(SpeakerEnemy, DialogSource.ENEMIES, "Wait low level skeletons get healing? I had to buy the deluxe ultra pass bundle to unlock healing, this is a scam")
            }
        ),
            new Dialog(
            30,
            () => GameState.RoundEvents.Contains(InterestingEvents.ENEMY_USED_HEALING),
            new List<Line>() {
                new Line(SpeakerPlayer1, DialogSource.PLAYERS, "What? The players are healing?"),
                new Line(SpeakerPlayer3, DialogSource.PLAYERS, "Yeah in that update the devs added like Saturday night at 11pm healing got introduced"),
                new Line(SpeakerPlayer2, DialogSource.PLAYERS, "Unfair, this fight is already hard")
            }
        ),
            new Dialog(
            90,
            () =>  GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "TREMBLE BEFORE ME I AM YOUR GOD"),
                new Line(SpeakerBoss, DialogSource.BOSS, "Well not actually thats the dev's but"),
            }
        ),  
            new Dialog(
            90,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_USED_HEALING) && GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "YOU THINK HEALING WILL HELP?"),
                new Line(SpeakerBoss, DialogSource.BOSS, "I WILL  C R U S H  YOU ALL THE SAME")
            }
        ),
            new Dialog(
            90,
            () => GameState.RoundEvents.Contains(InterestingEvents.PLAYER_GOT_KILL) && GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "Wow those attacks are better than I thought"),
            }
        ),
            new Dialog(
            90,
            () => GameState.RoundEvents.Contains(InterestingEvents.ENEMY_GOT_KILL) && GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "WHAT DID I TELL YOU? YOU ARE WEAK")
            }
        ),
            new Dialog(
            90,
            () => GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "YOU CAN NEVER DEFEAT ME")
            }
        ),
         new Dialog(
            90,
            () =>  GameState.RoundEvents.Contains(InterestingEvents.ENEMY_USED_HEALING) && GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "THINK THAT HURT ME? ILL JUST HEAL AWAY EVERYTHING YOU HAVE DONE, YOU CAN'T BEAT ME")
            }
        ),
            new Dialog(
            90,
            () =>  GameState.CurrentFight >= 7,
            new List<Line>() {
                new Line(SpeakerBoss, DialogSource.BOSS, "THIS IS MY FLOWER FIELD AND I WILL NOT LET YOU TARNISH ITS PEACFUL RELAXING MOOD")
            }
        )

    };

    public static Dialog NextDialog()
    {
        List<Dialog> validDialogs = new List<Dialog>();
        foreach (var dialog in Dialogs)
        {
            if (dialog.Condition())
                validDialogs.Add(dialog);
        }

        if (validDialogs.Count == 0)
            return null;
        
        int maxImportance = 0;
        Dialog maxImportanceDialog = null;
        foreach (var dialog in validDialogs)
        {
            if (dialog.Importance > maxImportance)
            {
                maxImportance = dialog.Importance;
                maxImportanceDialog = dialog;
            }
        }

        Dialogs.Remove(maxImportanceDialog);
        return maxImportanceDialog;
    }
}
