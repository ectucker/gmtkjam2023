public enum InterestingEvents
{
    PLAYER_USED_HEALING,
    ENEMY_USED_HEALING,
    PLAYER_GOT_KILL,
    ENEMY_GOT_KILL
}
