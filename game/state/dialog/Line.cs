using System;

public class Line
{
    public string Speaker { get; private set; }

    public DialogSource SpeakerSource { get; private set; }

    public string Text { get; private set; }

    public Action SideEffect { get; private set; }

    public Line(string speaker, DialogSource speakerSource, string text)
    {
        this.Speaker = speaker;
        this.SpeakerSource = speakerSource;
        this.Text = text;
        this.SideEffect = () => {};
    }

    public Line(string speaker, DialogSource speakerSource, string text, Action sideEffect)
    {
        this.Speaker = speaker;
        this.SpeakerSource = speakerSource;
        this.Text = text;
        this.SideEffect = sideEffect;
    }
}
