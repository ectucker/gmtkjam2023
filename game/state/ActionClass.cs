public enum ActionClass
{
    ATTACK,
    BUFF,
    DEBUFF,
    HEAL
}