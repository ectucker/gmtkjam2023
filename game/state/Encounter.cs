using System.Collections.Generic;

public class Encounter
{
    public List<Entity> Enemies { get; private set; }

    public Encounter(Entity enemy1, Entity enemy2, Entity enemy3)
    {
        this.Enemies = new List<Entity> {
            enemy1,
            enemy2,
            enemy3
        };
    }
}
