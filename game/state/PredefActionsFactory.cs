using System.Collections.Generic;

public static class PredefActionsFactory
{
    public static CombatAction GenericSkeletonAttack2(int level)
    {
        return new CombatAction(
            "generic_skeleton_attack_2",
            ActionClass.ATTACK,
            level,
            new List<IActionEffect> {
                new DamageActionEffect(level * 3)
            },
            TargetPredicate.OnlyOpposingTeam
        );
    }

    public static CombatAction CoolSwordSpin(int level)
    {
        return new CombatAction(
            "Cool Sword Spin",
            ActionClass.ATTACK,
            level,
            new List<IActionEffect> {
                new DamageActionEffect(level * 5)
            },
            TargetPredicate.OnlyOpposingTeam
        );
    }

    public static CombatAction TestDOT(int level)
    {
        return new CombatAction(
            "Test Damage Over Time",
            ActionClass.DEBUFF,
            level,
            new List<IActionEffect> {
                new DamageOverTimeEffect(level, 5)
            },
            TargetPredicate.OnlyOpposingTeam
        );
    }

    public static CombatAction BossAttack1(int level)
    {
        return new CombatAction(
            "Ultra Mega Super Wave of Turbo Death",
            ActionClass.ATTACK,
            level,
            new List<IActionEffect>{
                new AOEDamageOverTimeEffect(100, 3),
                new DamageDownEffect(5),
                new DamageDownEffect(5)
            },
            TargetPredicate.OnlyOpposingTeam
        );
    }

    public static CombatAction BossAttack2(int level){
            return new CombatAction(
            "Otherworld Healing Of Super Might",
            ActionClass.BUFF,
            level,
            new List<IActionEffect>{
               new HealEffect(100),
               new HealOverTimeEffect(50, 3),
               new AOEHealOverTimeEffect(35, 3),
               new AOEHealEffect(100)
            },
            TargetPredicate.OnlySameTeam
        );
    }

        public static CombatAction BossAttack3(int level){
            return new CombatAction(
            "SUPER BEAM OF DESTRUCTO DEATH",
            ActionClass.ATTACK,
            level,
            new List<IActionEffect>{
                new DefenseDownEffect(5),
                new DamageActionEffect(250)
            },
            TargetPredicate.OnlyOpposingTeam
        );
        }

        public static CombatAction BossAttack4(int level){
            return new CombatAction(
            "POWER WORD FUCK YOU",
            ActionClass.ATTACK,
            level,
            new List<IActionEffect>{
                new AOEDamageOverTimeEffect(150, 2)
            },
            TargetPredicate.OnlyOpposingTeam
        );
        }
        public static CombatAction SkelleAttack1(int level){
            return new CombatAction(
            "Super mega strength buff of powerful buffs",
            ActionClass.BUFF,
            level,
            new List<IActionEffect>{
               new DamageUpEffect(25)
            },
            TargetPredicate.OnlySameTeam
        );
        }

        public static CombatAction SkelleAttack2(int level){
            return new CombatAction(
            "Ultra defense buff of super buffs",
            ActionClass.BUFF,
            level,
            new List<IActionEffect>{
               new DefenseUpEffect(25)
            },
            TargetPredicate.OnlySameTeam
        );
        }

        public static CombatAction SkelleAttack3(int level){
            return new CombatAction(
            "Searing Ball Of ULTRA DEATH",
            ActionClass.BUFF,
            level,
            new List<IActionEffect>{
               new AOEDamageEffect(75),
               new AOEDamageOverTimeEffect(50, 2)
            },
            TargetPredicate.OnlyOpposingTeam
        );
        }

    
}
