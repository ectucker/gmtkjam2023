using Godot;

public class DamageUpStatus : AStatusEffect
{
    public int DamagePerTick { get; private set; }

    public DamageUpStatus(Entity target, int duration, int damagePerTick) : base(target, duration)
    {
        
    }

    public override void Apply()
    {
        Target.damageUp += Duration;
    }

    public override string TickEffect()
    {
        Target.damageUp -= 1;
        if (Duration == 0)
        {
            return $"{Target.Name}'s damage buff wore off.";
        }
        else
        {
            return "";
        }
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will have a damage buff ({Duration} rounds remaining).";
    }
}
