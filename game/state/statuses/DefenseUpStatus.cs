using Godot;

public class DefenseUpStatus : AStatusEffect
{
    public int DamagePerTick { get; private set; }

    public DefenseUpStatus(Entity target, int duration, int damagePerTick) : base(target, duration)
    {
        
    }

    public override void Apply()
    {
        Target.defenseUp += Duration;
    }

    public override string TickEffect()
    {
        Target.defenseUp -= 1;
        if (Duration == 0)
        {
            return $"{Target.Name}'s defense buff wore off.";
        }
        else
        {
            return "";
        }
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will have a defense buff ({Duration} rounds remaining).";
    }
}
