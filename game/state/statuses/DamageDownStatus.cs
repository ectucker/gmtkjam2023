using Godot;

public class DamageDownStatus : AStatusEffect
{
    public int DamagePerTick { get; private set; }

    public DamageDownStatus(Entity target, int duration, int damagePerTick) : base(target, duration)
    {
        
    }

    public override void Apply()
    {
        Target.damageDown += Duration;
    }

    public override string TickEffect()
    {
        Target.damageDown -= 1;
        if (Duration == 0)
        {
            return $"{Target.Name}'s damage debuff wore off.";
        }
        else
        {
            return "";
        }
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will have a damage debuff ({Duration} rounds remaining).";
    }
}
