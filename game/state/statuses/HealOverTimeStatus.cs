using Godot;

public class HealOverTimeStatus : AStatusEffect
{
    public int HealPerTick { get; private set; }

    public HealOverTimeStatus(Entity target, int duration, int HealPerTick) : base(target, duration)
    {
        this.HealPerTick = HealPerTick;
    }

    public override void Apply()
    {

    }

    public override string TickEffect()
    {
        Target.Heal(HealPerTick);
        return $"{Target.Name} was healed {HealPerTick} points of health by their status.";
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will be healed {HealPerTick} health points each round. ({Duration} rounds remaining).";
    }
}
