using Godot;

public class DefenseDownStatus : AStatusEffect
{
    public int DamagePerTick { get; private set; }

    public DefenseDownStatus(Entity target, int duration, int damagePerTick) : base(target, duration)
    {
        
    }

    public override void Apply()
    {
        Target.defenseDown += Duration;
    }

    public override string TickEffect()
    {
        Target.defenseDown -= 1;
        if (Duration == 0)
        {
            return $"{Target.Name}'s defense debuff wore off.";
        }
        else
        {
            return "";
        }
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will have a defense debuff ({Duration} rounds remaining).";
    }
}
