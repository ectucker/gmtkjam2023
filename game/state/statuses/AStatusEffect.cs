using Godot;

public abstract class AStatusEffect
{
    public int Duration { get; private set; }

    public Entity Target { get; private set; }

    public AStatusEffect(Entity target, int duration)
    {
        this.Target = target;
        this.Duration = duration;
    }

    public string Tick()
    {
        Duration -= 1;
        return TickEffect();
    }

    public abstract void Apply();

    public abstract string TickEffect();

    public abstract void Remove();

    public abstract string Describe();

    public bool CanCombine(AStatusEffect other)
    {
        return other.GetType() == this.GetType();
    }
}
