using Godot;

public class DamageOverTimeStatus : AStatusEffect
{
    public int DamagePerTick { get; private set; }

    public DamageOverTimeStatus(Entity target, int duration, int damagePerTick) : base(target, duration)
    {
        this.DamagePerTick = damagePerTick;
    }

    public override void Apply()
    {

    }

    public override string TickEffect()
    {
        Target.Damage(DamagePerTick);
        return $"{Target.Name} was dealt {DamagePerTick} damage by their status.";
    }

    public override void Remove()
    {

    }

    public override string Describe()
    {
        return $"Will be dealt {DamagePerTick} damage each round. ({Duration} rounds remaining).";
    }
}
