public enum DialogSource
{
    MESSAGE,
    PLAYERS,
    ENEMIES,
    BOSS
}