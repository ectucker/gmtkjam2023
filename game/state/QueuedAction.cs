using System.Text;

public class QueuedAction
{
    public CombatAction Action { get; private set; }

    public Entity Target { get; private set; }

    public QueuedAction(CombatAction action, Entity target)
    {
        this.Action = action;
        this.Target = target;
    }

    public bool IsValid(Entity user)
    {
        if (Target == null || user == null)
            return true;
        return Action.IsVaidTarget(Target, user);
    }

    public string Execute(Entity user)
    {
        StringBuilder builder = new StringBuilder();
        foreach (var effect in Action.Effects)
        {
            if (builder.Length > 0)
                builder.Append(", ");
            builder.Append(effect.Apply(Target, user));
        }
        builder.Append(".");
        switch (Action.ActionClass)
        {
            case ActionClass.ATTACK:
                GlobalSounds.AttackSounds.Play();
                break;
            case ActionClass.HEAL:
                GlobalSounds.HealSounds.Play();
                break;
            case ActionClass.BUFF:
                GlobalSounds.BuffSounds.Play();
                break;
            case ActionClass.DEBUFF:
                GlobalSounds.DebuffSounds.Play();
                break;
        }
        return builder.ToString();
    }
}
