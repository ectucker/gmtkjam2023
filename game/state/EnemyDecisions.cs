using System.Collections.Generic;
using Godot;

public static class EnemyDecsions
{
    public static void PickActions()
    {
        foreach (var entity in GameState.Enemies)
        {
            if (entity.Health > 0)
            {
                entity.QueuedAction = PickAction(entity);
            }
            else
            {
                entity.QueuedAction = null;
            }
        }
    }

    public static QueuedAction PickAction(Entity entity)
    {
        List<CombatAction> validActions = new List<CombatAction>();
        foreach (var action in entity.Actions)
        {
            if (GameState.HasValidTargets(action, entity))
                validActions.Add(action);
        }

        if (validActions.Count == 0)
            return null;

        CombatAction chosenAction = validActions[(int) (GD.Randi() % validActions.Count)];
        List<Entity> validTargets = new List<Entity>(GameState.ValidTargets(chosenAction, entity));
        Entity chosenTarget = validTargets[(int) (GD.Randi() % validTargets.Count)];
        return new QueuedAction(chosenAction, chosenTarget);
    }
}
