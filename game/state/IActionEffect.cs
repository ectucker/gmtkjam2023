public abstract class IActionEffect
{
    public abstract bool RequiresTarget();
    public abstract string Apply(Entity target, Entity user);

    public abstract ActionClass actionClass {get; set;}

    public bool CanCombine(IActionEffect other)
    {
        return other.GetType() == this.GetType();
    }

    public abstract IActionEffect Combine(IActionEffect other);
}
