using System.Collections.Generic;
using Godot;

public static class GameState
{
    /// GAMEPLAY STATE

    public static GameMode Mode { get; set; } = GameMode.COMBAT_START;

    public static List<Entity> Players = new List<Entity>(new Entity[]
    {
        new Entity(Team.PLAYER, "Gray Vyard", 100, new List<CombatAction> { PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2) }),
        new Entity(Team.PLAYER, "Helter Skeletor", 100, new List<CombatAction> { PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2) }),
        new Entity(Team.PLAYER, "McRibs", 100, new List<CombatAction> { PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2), PredefActionsFactory.GenericSkeletonAttack2(2) }),
    });

    // NB: These will imediately be replaced by the ones in EncounterList
    public static List<Entity> Enemies = new List<Entity>(new Entity[]
    {
        new Entity(Team.ENEMY, "XXX_CoolDude", 15, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(10) }),
        new Entity(Team.ENEMY, "Tilda", 15, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(10) }),
        new Entity(Team.ENEMY, "!@#&(*$(*))", 15, new List<CombatAction> { PredefActionsFactory.CoolSwordSpin(10) })
    });

    public static List<Entity> TurnOrder = new List<Entity>()
    {
        Players[0],
        Enemies[0],
        Players[1],
        Enemies[1],
        Players[2],
        Enemies[2]
    };

    /// PROGRESSION & DIALOG

    public static HashSet<CombatAction> EnemyUsedActions = new HashSet<CombatAction>();

    public static List<CombatAction> AvailableLoot = new List<CombatAction>();

    public static Dialog CurrentDialog = null;

    public static int CurrentFight { get; private set; } = 0;

    public static HashSet<InterestingEvents> RoundEvents = new HashSet<InterestingEvents>();

    public static HashSet<InterestingEvents> FightEvents = new HashSet<InterestingEvents>();

    /// UI STATE

    public static string Message { get; set; } = "";

    public static Entity SelectedEntity { get; set; }

    public static CombatAction SelectedAction { get; set; }

    public static Entity SelectedActionOwner { get; set; }

    public static Entity HoveredEntity { get; set; }

    public static CombatAction HoveredAction { get; set; }

    public static bool NeedsTarget { get; set; }

    /// DERIVED STATE

    public static IEnumerable<Entity> AllEntities()
    {
        foreach (var entity in Players)
            yield return entity;
        foreach (var entity in Enemies)
            yield return entity;
    }

    public static IEnumerable<Entity> AllLivingEntities()
    {
        foreach (var entity in AllEntities())
        {
            if (entity.Health > 0)
            {
                yield return entity;
            }
        }
    }

    public static bool HasValidTargets(CombatAction action, Entity user)
    {
        if (action == null || user == null)
            return false;

        foreach (var entity in AllLivingEntities())
        {
            if (action.IsVaidTarget(entity, user))
                return true;
        }
        return false;
    }

    public static IEnumerable<Entity> ValidTargets(CombatAction action, Entity user)
    {
        if (action == null || user == null)
            yield break;

        foreach (var entity in AllLivingEntities())
        {
            if (action.IsVaidTarget(entity, user))
                yield return entity;
        }
    }

    /// ACTIONS

    public static void NextRound(int round)
    {
        CurrentDialog = Script.NextDialog();
        RoundEvents.Clear();
        TurnOrder.Clear();
        foreach (var entity in AllEntities())
        {
            entity.QueuedAction = null;
            if (entity.Health > 0)
            {
                TurnOrder.Add(entity);
            }
        }
        Shuffle(TurnOrder);
        EnemyDecsions.PickActions();
        RoundDamage(round);
        SelectNextNeeded();
    }

    public static void RoundDamage(int round){
        if(round > 10){
            foreach(var enemy in GameState.Enemies)
                enemy.AddStatus(new DamageOverTimeStatus(enemy,1, round * 2));

            foreach(var player in GameState.Players)
                player.AddStatus(new DamageOverTimeStatus(player, 1, round * 2));
        }
    }


    public static void Shuffle<T>(List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            int k = (int) (GD.Randi() % n);
            n--;
            T temp = list[n];
            list[n] = list[k];
            list[k] = temp;
        }
    }

    public static void SelectNextNeeded()
    {
        if (Mode == GameMode.SELECT_ATTACKS)
        {
            if (NeedsTarget)
            {
                foreach (var target in ValidTargets(SelectedAction, SelectedActionOwner))
                {
                    var rep = CombatScreen.Instance.GetRep(target);
                    if (rep != null)
                    {
                        rep.GrabFocus();
                        return;
                    }
                }
            }

            foreach (var player in Players)
            {
                if (player.Health > 0 && player.QueuedAction == null)
                {
                    SelectedEntity = player;
                    SelectedAction = null;
                    SelectedActionOwner = player;
                    Message = $"Select an action for {player.Name}.";
                    GameStateHolder.NotifyChanged();
                    CombatScreen.Instance.GetActionButton(player.Actions[0])?.GrabFocus();
                    return;
                }
            }

            GameStateHolder.NotifyChanged();
            CombatScreen.Instance.AdvanceButton.GrabFocus();
            Message = "Commence the fighting!";
        }
    }

    public static void MarkEvent(InterestingEvents ie)
    {
        RoundEvents.Add(ie);
        FightEvents.Add(ie);
    }

    public static void AdvanceDialog()
    {
        if (CurrentDialog != null)
        {
            CurrentDialog.CurrentIndex += 1;
            if (CurrentDialog.Lines.Count <= CurrentDialog.CurrentIndex)
            {
                CurrentDialog = null;
                SelectNextNeeded();
            }
        }
    }

    public static void FinishFight()
    {
        SelectedAction = null;
        SelectedActionOwner = null;
        HoveredAction = null;
        HoveredEntity = null;

        AvailableLoot.Clear();
        List<CombatAction> usedActions = new List<CombatAction>(EnemyUsedActions);
        EnemyUsedActions.Clear();
        for (int i = 0; i < 4; ++i)
        {
            if (usedActions.Count > 0)
            {
                int pick = (int) (GD.Randi() % usedActions.Count);
                AvailableLoot.Add(usedActions[pick]);
                usedActions.RemoveAt(pick);
            }
        }
        
        CurrentFight += 1;

        CurrentDialog = Script.NextDialog();
        RoundEvents.Clear();
        FightEvents.Clear();
        foreach (var player in Players)
        {
            player.ResetStatus();
        }
    }

    public static void StartEncounter()
    {
        var encounter = EncounterList.NextEncounter();
        if (encounter != null)
        {
            Mode = GameMode.COMBAT_START;
            Enemies[0] = encounter.Enemies[0];
            Enemies[1] = encounter.Enemies[1];
            Enemies[2] = encounter.Enemies[2];
        }
        else
        {
            Mode = GameMode.FINISHED;
        }
    }

    public static bool PlayersWon()
    {
        foreach (var enemy in Enemies)
        {
            if (enemy.Health > 0)
                return false;
        }
        return true;
    }

    public static bool PlayersLost()
    {
        foreach (var player in Players)
        {
            if (player.Health > 0)
                return false;
        }
        return true;
    }
}
