using Godot;
using System;

/// <summary>
/// A game wide pause menu. Handles showing and hiding using it's own UI action.
/// Also handles the toggle fullscreen action.
/// 
/// Should be disabled on other menus.
/// </summary>
public class VolumeContainer : Control
{
    private Button _resumeButton;
    private Button _exitButton;

    private Range _mainVolumeRange;
    private Range _effectsVolumeRange;
    private Range _musicVolumeRange;

    private AnimationPlayer _animPlayer;
    
    public override void _Ready()
    {
        base._Ready();
        
        _mainVolumeRange = FindNode("MainSlider") as Range;
        _mainVolumeRange.Value = AudioSettings.MainVolume;
        _mainVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
        _effectsVolumeRange = FindNode("EffectsSlider") as Range;
        _effectsVolumeRange.Value = AudioSettings.EffectsVolume;
        _effectsVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
        _musicVolumeRange = FindNode("MusicSlider") as Range;
        _musicVolumeRange.Value = AudioSettings.MusicVolume;
        _musicVolumeRange.Connect(SignalNames.RANGE_VALUE_CHANGED, this, nameof(UpdateVolumes));
    }

    public override void _Input(InputEvent inputEvent)
    {
        base._Input(inputEvent);

        if (inputEvent.IsActionPressed("fullscreen"))
        {
            OS.WindowFullscreen = !OS.WindowFullscreen;
        }
    }

    private void UpdateVolumes(float _)
    {
        AudioSettings.MainVolume = (float)_mainVolumeRange.Value;
        AudioSettings.EffectsVolume = (float)_effectsVolumeRange.Value;
        AudioSettings.MusicVolume = (float)_musicVolumeRange.Value;
    }
}
