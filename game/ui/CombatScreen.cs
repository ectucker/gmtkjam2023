using Godot;
using System;
using System.Collections.Generic;

public class CombatScreen : Control
{
    public static CombatScreen Instance;

    private Dictionary<Entity, EntityRep> _repMap = new Dictionary<Entity, EntityRep>();

    private List<ActionButton> _actionButtons = new List<ActionButton>();

    public AdvanceButton AdvanceButton;

    public override void _Ready()
    {
        Instance = this;
        AdvanceButton = this.FindChild<AdvanceButton>();
        _actionButtons = new List<ActionButton>(this.FindChildren<ActionButton>());
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
        GameState.StartEncounter();
        GameState.NextRound(0);
    }

    private void _StateChanged()
    {
        foreach (var entityRep in this.FindChildren<EntityRep>())
        {
            _repMap[entityRep.GetEntity()] = entityRep;
        }
    }

    public EntityRep GetRep(Entity entity)
    {
        if (_repMap.ContainsKey(entity))
            return _repMap[entity];
        return null;
    }

    public ActionButton GetActionButton(CombatAction action)
    {
        foreach (var ActionButton in _actionButtons)
        {
            if (ActionButton.GetAction() == action)
                return ActionButton;
        }
        return null;
    }
}
