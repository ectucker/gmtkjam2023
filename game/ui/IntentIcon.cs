using Godot;
using System;

public class IntentIcon : TextureRect
{
    private EntityRep _entity;

    private Texture _attackIcon;
    private Texture _buffIcon;
    private Texture _debuffIcon;
    private Texture _undecidedIcon;

    public override void _Ready()
    {
        base._Ready();

        _attackIcon = GD.Load<Texture>("res://game/ui/intents/intent_attack.png");
        _buffIcon = GD.Load<Texture>("res://game/ui/intents/team-upgrade.png");
        _debuffIcon = GD.Load<Texture>("res://game/ui/intents/poison-bottle.png");
        _undecidedIcon = GD.Load<Texture>("res://game/ui/intents/uncertainty.png");
        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (entity.QueuedAction != null)
        {
            switch (entity.QueuedAction.Action.ActionClass)
            {
                case ActionClass.ATTACK:
                    Texture = _attackIcon;
                    break;
                case ActionClass.BUFF:
                    Texture = _buffIcon;
                    break;
                case ActionClass.DEBUFF:
                    Texture = _debuffIcon;
                    break;
            }
        }
        else
        {
            Texture = _undecidedIcon;
        }
    }
}
