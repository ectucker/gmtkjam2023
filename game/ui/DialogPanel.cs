using Godot;
using System;

public class DialogPanel : PanelContainer
{
    private AnimationPlayer _animPlayer;

    private string _lastDialog = "";

    public override void _Ready()
    {
        base._Ready();

        _animPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        Visible = !GameState.Message.Empty() || GameState.CurrentDialog != null;
        if (GameState.CurrentDialog != null && GameState.CurrentDialog.CurrentLine != null && !_lastDialog.Equals(GameState.CurrentDialog.CurrentLine.Text))
        {
            _animPlayer.Play("ShowDialog");
            _lastDialog = GameState.CurrentDialog.CurrentLine.Text;
        }
    }

    public override void _Input(InputEvent @event)
    {
        if (GameState.CurrentDialog != null)
        {
            if (@event.IsActionPressed("ui_accept"))
            {
                GameState.AdvanceDialog();
                GameStateHolder.NotifyChanged();
                AcceptEvent();
            }
            else if (@event is InputEventMouseButton mouseEvent && mouseEvent.ButtonIndex == 1 && mouseEvent.Pressed)
            {
                GameState.AdvanceDialog();
                GameStateHolder.NotifyChanged();
                AcceptEvent();
            }
        }
    }
}
