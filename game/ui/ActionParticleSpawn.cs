using Godot;
using System;
using System.Collections.Generic;

public class ActionParticleSpawn : Node2D
{
    List<PackedScene> _attackParticles = new List<PackedScene>();
    List<PackedScene> _buffParticles = new List<PackedScene>();
    List<PackedScene> _debuffParticles = new List<PackedScene>();
    List<PackedScene> _healParticles = new List<PackedScene>();

    public override void _Ready()
    {
        base._Ready();

        _attackParticles.Add(GD.Load<PackedScene>("res://game/ui/particles/damage/groundexplosion1.tscn"));

        _debuffParticles.Add(GD.Load<PackedScene>("res://game/ui/particles/debuff/debuff1.tscn"));

        _buffParticles.Add(GD.Load<PackedScene>("res://game/ui/particles/buff/buff1.tscn"));

        _healParticles.Add(GD.Load<PackedScene>("res://game/ui/particles/heal/heal1.tscn"));
    }

    public void SpawnParticle(ActionClass actionClass)
    {
        foreach (Node child in GetChildren())
        {
            RemoveChild(child);
            child.QueueFree();
        }

        PackedScene particleScene;
        switch (actionClass)
        {
            case ActionClass.BUFF:
                particleScene = _buffParticles[(int) (GD.Randi() % _buffParticles.Count)];
                break;
            case ActionClass.DEBUFF:
                particleScene = _debuffParticles[(int) (GD.Randi() % _debuffParticles.Count)];
                break;
            case ActionClass.HEAL:
                particleScene = _healParticles[(int) (GD.Randi() % _healParticles.Count)];
                break;
            case ActionClass.ATTACK:
            default:
                particleScene = _attackParticles[(int) (GD.Randi() % _attackParticles.Count)];
                break;
        }

        Node2D node = particleScene.Instance<Node2D>();
        AddChild(node);
        CallDeferred(nameof(StartParticles));
    }

    private void StartParticles()
    {
        foreach (var particle in this.FindChildren<CPUParticles2D>())
        {
            particle.Emitting = true;
        }
    }
}
