using Godot;
using System;

public class AvailableAsTargetParticles : ParticlesHandler
{
    private EntityRep _entity;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (GameState.NeedsTarget && GameState.SelectedAction.IsVaidTarget(entity, GameState.SelectedActionOwner))
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
}
