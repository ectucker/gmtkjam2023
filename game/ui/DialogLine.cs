using Godot;
using System;

public class DialogLine : RichTextLabel
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameState.CurrentDialog != null)
        {
            Text = GameState.CurrentDialog.CurrentLine.Text;
        }
        else
        {
            Text = GameState.Message;
        }
    }
}
