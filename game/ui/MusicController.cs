using Godot;
using System;

public class MusicController : Node
{
    private AudioStreamPlayer _introMusic;
    private AudioStreamPlayer _mainMusic;
    private AudioStreamPlayer _bossMusic;

    public override void _Ready()
    {
        base._Ready();

        _introMusic = GetNode<AudioStreamPlayer>("IntroMusic");
        _mainMusic = GetNode<AudioStreamPlayer>("MainMusic");
        _bossMusic = GetNode<AudioStreamPlayer>("BossMusic");

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        // Boss battle
        if (GameState.CurrentFight >= 7)
        {
            if (GameState.Mode == GameMode.SELECT_ATTACKS)
            {
                if (!_bossMusic.Playing)
                    _bossMusic.Play();
                _introMusic.Stop();
                _mainMusic.Stop();
            }
        }
        else if (GameState.CurrentFight >= 1)
        {
            if (GameState.Mode == GameMode.SELECT_ATTACKS)
            {
                if (!_mainMusic.Playing)
                    _mainMusic.Play();
                _introMusic.Stop();
                _bossMusic.Stop();
            }
        }
        else
        {
            if (!_introMusic.Playing)
                _introMusic.Play();
            _bossMusic.Stop();
            _mainMusic.Stop();
        }
    }
}
