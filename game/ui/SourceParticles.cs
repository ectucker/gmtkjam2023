using Godot;
using System;

public class SourceParticles : ParticlesHandler
{
    private EntityRep _entity;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (!GameState.NeedsTarget && GameState.HoveredEntity != null && GameState.HoveredEntity == entity && entity.QueuedAction != null)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
}
