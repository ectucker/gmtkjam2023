using Godot;
using System;

public class LootPanel : PanelContainer
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        bool shown = GameState.Mode == GameMode.LOOT;
        if (shown)
        {
            Modulate = Colors.White;
        }
        else
        {
            Modulate = Colors.Transparent;
        }
        foreach (CanvasItem child in GetChildren())
        {
            child.Visible = shown;
        }
    }
}
