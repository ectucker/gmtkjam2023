using Godot;
using System;

public class IntentDisplay : VBoxContainer
{
    [Export]
    public string TeamGroup = "Player";

    private Team _team;

    private EntityRep _entity;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        if (TeamGroup == "Player")
        {
            _team = Team.PLAYER;
        }
        else
        {
            _team = Team.ENEMY;
        }
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        Visible = entity.Health > 0 && entity.Team == _team;
    }
}
