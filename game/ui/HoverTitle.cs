using Godot;
using System;

public class HoverTitle : Label
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameState.HoveredAction != null)
        {
            Text = GameState.HoveredAction.Name;
        }
        else if (GameState.HoveredEntity != null)
        {
            Text = GameState.HoveredEntity.Name;
        }
        else
        {
            Text = "";
        }
    }
}
