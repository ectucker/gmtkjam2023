using Godot;
using System;
using System.Collections.Generic;

public class EnemySprite : Node2D
{
    private EntityRep _entity;
    
    private Sprite _head;

    private List<Texture> _headTextures = new List<Texture>();

    private string _lastName = "";

    public override void _Ready()
    {
        base._Ready();

        LoadHeadTexture();
        _entity = this.FindParent<EntityRep>();
        _head = GetNode<Sprite>("Head");
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void LoadHeadTexture()
    {
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head1.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head2.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head3.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head4.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head5.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head6.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head7.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head8.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head9.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head10.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head11.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head12.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head13.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head14.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head15.png"));
        _headTextures.Add(GD.Load<Texture>("res://game/sprites/enemy_sprites/head/head16.png"));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (entity != null && entity.Team == Team.ENEMY)
        {
            Visible = true;
            if (entity.Name != _lastName)
            {
                _lastName = entity.Name;
                _head.Texture = _headTextures[(int) (GD.Randi() % _headTextures.Count)];
            }
            RotationDegrees = entity.Health > 0 ? 0 : 90;
        }
        else
        {
            Visible = false;
        }
    }
}
