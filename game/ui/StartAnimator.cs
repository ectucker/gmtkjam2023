using Godot;
using System;

public class StartAnimator : Node
{
    private GameMode _lastMode = GameMode.MAIN_MENU;

    public override void _Ready()
    {
        base._Ready();
        
        foreach (var player in GameState.Players)
            player.RecalcHealthSkeleton();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameState.Mode == GameMode.COMBAT_START && _lastMode != GameMode.COMBAT_START)
        {
            if (GameState.CurrentDialog == null)
            {
                GameState.Mode = GameMode.SELECT_ATTACKS;
                GameState.NextRound(0);
                GameState.SelectNextNeeded();
                GameStateHolder.NotifyChanged();
            }
        }
        else if (GameState.Mode != GameMode.COMBAT_START)
        {
            _lastMode = GameState.Mode;
        }
    }
}
