using Godot;
using System;

public class PlayerSprite : Sprite
{
    private EntityRep _entity;
    

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private string GetFolder(string name)
    {
        if (name.Contains("Mc"))
        {
            return "res://game/sprites/player_sprites/player_3/";
        }
        if (name.Contains("Gray"))
        {
            return "res://game/sprites/player_sprites/player_1/";
        }
        if (name.Contains("Hel"))
        {
            return "res://game/sprites/player_sprites/player_2/";
        }
        else
        {
            return "";
        }
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (entity != null && entity.Team == Team.PLAYER)
        {
            Visible = true;
            string path = GetFolder(entity.Name);
            if (entity.Health > 0)
            {
                Texture = GD.Load<Texture>(path + "skelle.png");
            }
            else
            {
                Texture = GD.Load<Texture>(path + "skelle_dead.png");
            }
        }
        else
        {
            Visible = false;
        }
    }
}
