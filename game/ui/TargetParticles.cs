using Godot;
using System;

public class TargetParticles : ParticlesHandler
{
    private EntityRep _entity;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (!GameState.NeedsTarget && GameState.HoveredEntity != null && GameState.HoveredEntity.QueuedAction != null && GameState.HoveredEntity.QueuedAction.Target == entity)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
}
