using Godot;
using System;

public class ActionButton : Button
{
    [Export]
    public string Category = "Player";

    [Export]
    public int Index = 0;

    public override void _Ready()
    {
        base._Ready();
        
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
        Connect(SignalNames.CONTROL_FOCUS_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_FOCUS_EXITED, this, nameof(_UnHovered));
        Connect(SignalNames.CONTROL_MOUSE_EXITED, this, nameof(_UnHovered));
        _StateChanged();
    }

    public Entity GetEntity()
    {
        if (Category == "Player" && GameState.SelectedEntity?.Team == Team.PLAYER)
            return GameState.SelectedEntity;
        return null;
    }

    public CombatAction GetAction()
    {
        if (Category == "Player")
        {
            if (GameState.SelectedEntity?.Team == Team.PLAYER && GameState.SelectedEntity.Actions.Count > Index)
                return GameState.SelectedEntity.Actions[Index];
        }
        else if (Category == "Loot")
        {
            if (GameState.AvailableLoot.Count > Index)
                return GameState.AvailableLoot[Index];
        }

        return null;
    }

    private void _StateChanged()
    {
        if (GameState.CurrentDialog != null)
        {
            Disabled = true;
        }
        else
        {
            var action = GetAction();
            var entity = GetEntity();
            Visible = action != null;
            Text = $"{action?.Name} Lv. {action?.Level}";
            if (GameState.Mode == GameMode.LOOT)
            {
                Disabled = false;
            }
            else if (Category == "Player")
            {
                Disabled = GameState.Mode != GameMode.SELECT_ATTACKS || GameState.NeedsTarget || !GameState.HasValidTargets(action, entity);
            }
        }
        FocusMode = Disabled ? FocusModeEnum.None : FocusModeEnum.All;
    }

    private void _Hovered()
    {
        if (GameState.CurrentDialog == null)
        {
            GameState.HoveredAction = GetAction();
            GameState.HoveredEntity = GetEntity();
            GameStateHolder.NotifyChanged();
        }
    }

    private void _UnHovered()
    {
        if (GameState.CurrentDialog == null)
        {
            GameState.HoveredAction = GameState.HoveredAction == GetAction() ? null : GameState.HoveredAction;
            GameState.HoveredEntity = GameState.HoveredEntity == GetEntity() ? null : GameState.HoveredEntity;
            GameStateHolder.NotifyChanged();
        }
    }

    public override void _Pressed()
    {
        base._Pressed();

        if (GameState.Mode == GameMode.LOOT)
        {
            // Set as swap source
            if (GameState.SelectedAction == null)
            {
                var entity = GetEntity();
                var action = GetAction();
                GameState.SelectedAction = action;
                GameState.SelectedActionOwner = entity;
                if (entity != null)
                {
                    GameState.Message = $"Pick an Action to Swap with {entity.Name}'s {action.Name} Lv. {action.Level}.";
                }
                else
                {
                    GameState.Message = $"Pick an Action to Swap with {action.Name} Lv. {action.Level}.";
                }
            }
            // Actually swap
            else
            {
                var action1 = GetAction();
                var ownerList1 = GameState.AvailableLoot;
                if (Category == "Player")
                {
                    ownerList1 = GetEntity().Actions;
                }

                var action2 = GameState.SelectedAction;
                var ownerList2 = GameState.AvailableLoot;
                if (GameState.SelectedActionOwner != null)
                {
                    ownerList2 = GameState.SelectedActionOwner.Actions;
                }

                int index1 = ownerList1.IndexOf(action1);
                int index2 = ownerList2.IndexOf(action2);
                ownerList1[index1] = action2;
                ownerList2[index2] = action1;

                GameState.SelectedAction = null;
                GameState.SelectedActionOwner = null;

                GameState.Message = "Pick another action to swap. Or click finish.";

                foreach (var player in GameState.Players)
                    player.RecalcHealthSkeleton();
            }
        }
        else if (Category == "Player")
        {
            var action = GetAction();
            if (action != null)
            {
                GameState.SelectedAction = action;
                GameState.SelectedActionOwner = GetEntity();
                if (GameState.Mode == GameMode.SELECT_ATTACKS)
                {
                    GameState.Mode = GameMode.SELECT_ATTACKS;
                    GameState.NeedsTarget = action.RequiresTarget();
                    if (!action.RequiresTarget())
                    {
                        GameState.SelectedActionOwner.QueuedAction = new QueuedAction(action, null);
                    }
                    else
                    {
                        GameState.Message = $"Select target for {action.Name}.";
                    }
                    GameState.SelectNextNeeded();
                }
            }
        }
        GameStateHolder.NotifyChanged();
    }
}
