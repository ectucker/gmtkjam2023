using Godot;
using System;

public class AdvanceButton : Button
{
    public override void _Ready()
    {
        base._Ready();

        Connect(SignalNames.CONTROL_FOCUS_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_FOCUS_EXITED, this, nameof(_UnHovered));
        Connect(SignalNames.CONTROL_MOUSE_EXITED, this, nameof(_UnHovered));
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _Hovered()
    {
        if (!Disabled && GameState.Mode == GameMode.SELECT_ATTACKS)
        {
            GameState.Message = "Commence the fighting!";
        }
    }

    private void _UnHovered()
    {
        if (GameState.Message == "Commence the fighting!")
        {
            GameState.Message = "";
        }
    }

    private void _StateChanged()
    {
        if (GameState.CurrentDialog != null)
        {
            Disabled = true;
        }
        else if (GameState.Mode == GameMode.LOOT)
        {
            Text = "Finished";
            Disabled = false;
            Visible = true;
            Modulate = Colors.White;
        }
        else if (GameState.Mode == GameMode.SELECT_ATTACKS)
        {
            Text = "Ready";
            bool ready = true;
            foreach (var entity in GameState.Players)
            {
                if (entity.Health > 0 && entity.QueuedAction == null)
                    ready = false;
            }
            if (GameState.NeedsTarget)
                ready = false;
            Disabled = !ready;
            Modulate = Colors.White;
        }
        else
        {
            Modulate = Colors.Transparent;
            Disabled = true;
        }
        FocusMode = Disabled ? FocusModeEnum.None : FocusModeEnum.All;
    }

    public override void _Pressed()
    {
        if (GameState.Mode == GameMode.SELECT_ATTACKS)
        {
            GameState.Mode = GameMode.EXECUTE_ATTACKS;
        }
        else if (GameState.Mode == GameMode.LOOT)
        {
            GameState.StartEncounter();
        }
        GameStateHolder.NotifyChanged();
    }
}
