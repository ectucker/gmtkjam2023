using Godot;
using System;
using System.Collections.Generic;

public class ParticlesHandler : Node
{
    private List<CPUParticles2D> _particles;

    public override void _Ready()
    {
        base._Ready();

        _particles = new List<CPUParticles2D>(this.FindChildren<CPUParticles2D>());
    }

    public void Show()
    {
        foreach (var particle in _particles)
            particle.Emitting = true;
    }

    public void Hide()
    {
        foreach (var particle in _particles)
            particle.Emitting = false;
    }
}
