using Godot;
using System;

public class BossSprite : Sprite
{
    private EntityRep _entity;

    [Export]
    public bool BossMode = false;

    [Export]
    public bool BackupMode = true;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (entity != null && entity.Team == Team.ENEMY && entity.Name.Contains("Groxidius"))
        {
            Visible = BossMode;
        }
        else if (entity != null && entity.Team == Team.ENEMY && entity.Name.Contains("Skeleton"))
        {
            Visible = BackupMode;
        }
        else
        {
            Visible = !BossMode && !BackupMode;
        }
    }
}
