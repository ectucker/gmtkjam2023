using Godot;
using System;
using System.Text;

public class HoverDescription : Label
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private static string ClassVerb(ActionClass actionClass)
    {
        switch (actionClass)
        {
            case ActionClass.ATTACK:
                return "attack";
            case ActionClass.BUFF:
                return "buff";
            case ActionClass.DEBUFF:
                return "debuff";
            case ActionClass.HEAL:
                return "heal";
            default:
                return "act";
        }
    }

    public static string Order(int index)
    {
        switch (index)
        {
            case 0:
                return "1st";
            case 1:
                return "2nd";
            case 2:
                return "3rd";
            case 3:
                return "4th";
            case 4:
                return "5th";
            case 5:
                return "6th";
            default:
                return "sometime";
        }
    }

    private void _StateChanged()
    {
        if (GameState.HoveredAction != null)
        {
            StringBuilder desc = new StringBuilder();
            foreach (var effect in GameState.HoveredAction.Effects)
            {
                desc.AppendLine(effect.ToString());
            }
            Text = desc.ToString();
        }
        else if (GameState.HoveredEntity != null)
        {
            var entity = GameState.HoveredEntity;
            StringBuilder desc = new StringBuilder();
            if (entity.Health > 0)
            {
                desc.AppendLine($"{entity.Health}/{entity.MaxHealth} HP");
                if (GameState.TurnOrder.Contains(entity))
                {
                    int index = GameState.TurnOrder.IndexOf(entity);
                    desc.Append($"Will be acting {Order(index)}. ");
                }
                if (entity.QueuedAction != null)
                {
                    string verb = ClassVerb(entity.QueuedAction.Action.ActionClass);
                    if (entity.QueuedAction.Target != null)
                    {
                        desc.Append($"Planning to {verb} {entity.QueuedAction.Target.Name}.");
                    }
                    else
                    {
                        desc.Append($"Planning to {verb}.");
                    }
                }
                else
                {
                    desc.Append("Still needs to plan an action.");
                }
                desc.AppendLine();
                foreach (var status in entity.Statuses)
                {
                    desc.AppendLine($"{status.Describe()}");
                }
            }
            else
            {
                desc.AppendLine("They're dead.");
            }
            Text = desc.ToString();
        }
        else
        {
            Text = "";
        }
    }
}
