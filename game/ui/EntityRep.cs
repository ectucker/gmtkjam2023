using Godot;
using System;

public class EntityRep : TextureButton
{
    [Export]
    public string TeamCode = "Player";

    [Export]
    public int Index = 0;

    private Team _team;

    public ActionParticleSpawn ActionParticleSpawn;

    public AnimationPlayer AnimationPlayer;

    public override void _Ready()
    {
        _team = TeamCode == "Player" ? Team.PLAYER : Team.ENEMY;

        ActionParticleSpawn = this.FindChild<ActionParticleSpawn>();
        AnimationPlayer = this.FindChild<AnimationPlayer>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
        Connect(SignalNames.CONTROL_FOCUS_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_MOUSE_ENTERED, this, nameof(_Hovered));
        Connect(SignalNames.CONTROL_FOCUS_EXITED, this, nameof(_UnHovered));
        Connect(SignalNames.CONTROL_MOUSE_EXITED, this, nameof(_UnHovered));
    }

    public Entity GetEntity()
    {
        if (_team == Team.PLAYER)
            return GameState.Players[Index];
        else if (_team == Team.ENEMY)
            return GameState.Enemies[Index];
        return null;
    }

    private void _StateChanged()
    {
        var entity = GetEntity();
        Disabled = entity.Health <= 0;
        Visible = true;
        if (GameState.Mode == GameMode.FINISHED && _team == Team.ENEMY)
        {
            Disabled = true;
            Visible = false;
        }
        else if (GameState.Mode == GameMode.LOOT && _team == Team.ENEMY)
        {
            Disabled = true;
            Visible = false;
        }
        else if (GameState.CurrentDialog != null)
        {
            Disabled = true;
        }
        else if (GameState.Mode == GameMode.LOOT && _team == Team.PLAYER)
        {
            Disabled = false;
            Visible = true;
            Modulate = Colors.White;
        }
        else if (GameState.NeedsTarget)
        {
            if (GameState.SelectedAction.IsVaidTarget(entity, GameState.SelectedActionOwner) && GameState.HoveredEntity == entity)
            {
                Modulate = Colors.White;
                Disabled = false;
            }
            else if (GameState.SelectedAction.IsVaidTarget(entity, GameState.SelectedActionOwner))
            {
                Modulate = new Color(1.0f, 1.0f, 1.0f, 0.7f);
                Disabled = false;
            }
            else
            {
                Modulate = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                Disabled = true;
            }
        }
        else if (GameState.HoveredEntity != null)
        {
            if (GameState.HoveredEntity == entity)
            {
                Modulate = Colors.White;
            }
            else if (GameState.HoveredEntity.QueuedAction != null && GameState.HoveredEntity.QueuedAction.Target == entity)
            {
                Modulate = new Color(1.0f, 1.0f, 1.0f, 0.7f);;
            }
            else
            {
                Modulate = new Color(1.0f, 1.0f, 1.0f, 0.5f);
            }
        }
        else
        {
            Modulate = Colors.White;
        }
        FocusMode = Disabled ? FocusModeEnum.None : FocusModeEnum.All;
    }

    private void _Hovered()
    {
        if (GameState.CurrentDialog == null)
        {
            GameState.HoveredAction = null;
            GameState.HoveredEntity = GetEntity();
            GameStateHolder.NotifyChanged();
        }
    }

    private void _UnHovered()
    {
        if (GameState.CurrentDialog == null)
        {
            GameState.HoveredAction = null;
            GameState.HoveredEntity = GameState.HoveredEntity == GetEntity() ? null : GameState.HoveredEntity;
            GameStateHolder.NotifyChanged();
        }
    }

    public override void _Pressed()
    {
        var entity = GetEntity();
        if (GameState.Mode == GameMode.LOOT)
        {
            GameState.SelectedEntity = entity;
        }
        else if (GameState.Mode == GameMode.SELECT_ATTACKS && !GameState.NeedsTarget)
        {
            if (entity.Team == Team.PLAYER && entity.Health > 0)
            {
                GameState.SelectedAction = null;
                GameState.SelectedEntity = entity;
                GameState.Message = $"Select an action for {entity.Name}.";
            }
        }
        else if (GameState.Mode == GameMode.SELECT_ATTACKS && GameState.NeedsTarget && GameState.SelectedAction.IsVaidTarget(entity, GameState.SelectedActionOwner))
        {
            GameState.SelectedActionOwner.QueuedAction = new QueuedAction(GameState.SelectedAction, entity);
            GameState.NeedsTarget = false;
            GameState.Message = "";
            GameState.SelectNextNeeded();
        }
        GameStateHolder.NotifyChanged();
    }
}
