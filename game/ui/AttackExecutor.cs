using Godot;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

public class AttackExecutor : Node
{
    private bool _running = false;

    private int _round = 0;
    
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameState.Mode == GameMode.EXECUTE_ATTACKS && !_running)
        {
            AsyncRoutine.Start(this, RunAttacks);
        }
    }

    private async Task RunAttacks(AsyncRoutine routine)
    {
        try
        {
        _running = true;

        foreach (var entity in GameState.TurnOrder)
        {
            if (entity.Health > 0 && entity.QueuedAction != null && entity.QueuedAction.IsValid(entity))
            {
                // Start animation
                await routine.Delay(0.5f);

                // Actually execute attack
                try
                {
                    await routine.IdleFrame();
                    string effect = entity.QueuedAction.Execute(entity);
                    if (entity.Team == Team.ENEMY)
                        GameState.EnemyUsedActions.Add(entity.QueuedAction.Action);
                    GameState.Message = $"{entity.Name} used {entity.QueuedAction.Action.Name}, {effect}";
                    if (entity.QueuedAction.Target != null)
                    {
                        await routine.IdleFrame();
                        CombatScreen.Instance.GetRep(entity.QueuedAction.Target).ActionParticleSpawn.SpawnParticle(entity.QueuedAction.Action.ActionClass);
                    }
                    else
                    {
                        await routine.IdleFrame();
                        foreach (var target in GameState.ValidTargets(entity.QueuedAction.Action, entity))
                        {
                            CombatScreen.Instance.GetRep(target).ActionParticleSpawn.SpawnParticle(entity.QueuedAction.Action.ActionClass);
                        }
                    }
                    if (entity.Team == Team.ENEMY)
                    {
                        await routine.IdleFrame();
                        CombatScreen.Instance.GetRep(entity).AnimationPlayer.Play("AttackEnemy");
                    }
                    else
                    {
                        await routine.IdleFrame();
                        CombatScreen.Instance.GetRep(entity).AnimationPlayer.Play("AttackPlayer");
                    }
                    CameraEffects.Trauma += 0.5f;
                    GameStateHolder.NotifyChanged();
                } 
                catch (Exception e)
                {
                    GD.Print(e.ToString());
                    GD.Print(e.StackTrace);
                }


                // Finish animation
                await routine.Delay(0.5f);
            }
        }

        foreach (var entity in GameState.TurnOrder)
        {
            if (entity.Health > 0)
            {
                List<AStatusEffect> statusesCopy = new List<AStatusEffect>(entity.Statuses);
                foreach (var status in statusesCopy)
                {
                    if (entity.Health > 0)
                    {
                        try
                        {
                            string message = status.Tick();
                            if (status.Duration <= 0)
                                entity.RemoveStatus(status);
                            
                            if (!message.Empty())
                            {
                                GameState.Message = message;
                                GameStateHolder.NotifyChanged();
                                await routine.IdleFrame();
                                GlobalSounds.StatusSounds.Play();

                                await routine.Delay(1.0f);
                            }
                        } 
                        catch (Exception e)
                        {
                            GD.Print(e.ToString());
                            GD.Print(e.StackTrace);
                        }
                    }
                }
            }
        }

        if (GameState.PlayersLost())
        {
            GameState.Message = "You Were Slain by Players. Respawning...";
            GameStateHolder.NotifyChanged();

            await routine.Delay(3.0f);

            if (EncounterList.HasNextEncounter())
            {
                GameState.Mode = GameMode.LOOT;
                GameState.FinishFight();
                GameState.Message = "Pick an Action to Swap.";
                GameStateHolder.NotifyChanged();
            }
            else
            {
                GameState.Mode = GameMode.FINISHED;
                GameState.Message = "Game Finished!";
                GameState.CurrentDialog = Script.NextDialog();
                GameStateHolder.NotifyChanged();
            }
        }
        else if (GameState.PlayersWon())
        {
            GameState.Message = "You Defeated the Players";
            EncounterList.repeatedEncounter = null;
            GameStateHolder.NotifyChanged();

            await routine.Delay(3.0f);

            if (EncounterList.HasNextEncounter())
            {
                GameState.Mode = GameMode.LOOT;
                GameStateHolder.NotifyChanged();
                GameState.FinishFight();
                GameState.Message = "Pick an Action to Swap.";
                GameStateHolder.NotifyChanged();
            }
            else
            {
                GameState.Mode = GameMode.FINISHED;
                GameState.Message = "Game Finished!";
                GameState.CurrentDialog = Script.NextDialog();
                GameStateHolder.NotifyChanged();
            }
        }
        else
        {

            GameStateHolder.NotifyChanged();
            GameState.Mode = GameMode.SELECT_ATTACKS;
            _round += 1;            
            if(_round > 10){
                GameState.Message = $"MMO Devs want the fight to end soon so everyone is taking {_round*2} damage.";
                GameStateHolder.NotifyChanged();
                await routine.Delay(3.0f);
            }
            GameState.NextRound(_round);

            GameStateHolder.NotifyChanged();
        }
        _running = false;
        } 
        catch (Exception e)
        {
            GD.Print(e.ToString());
            GD.Print(e.StackTrace);
        }
    }
}
