using Godot;
using System;

public class DialogSpeaker : Label
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        Visible = GameState.CurrentDialog != null && GameState.CurrentDialog.CurrentLine.SpeakerSource != DialogSource.MESSAGE;
        if (GameState.CurrentDialog != null)
        {
            Text = GameState.CurrentDialog.CurrentLine.Speaker;
            if (GameState.CurrentDialog.CurrentLine.Speaker == "ENEMY_FILL_IN")
            {
                Text = GameState.Enemies[0].Name;
            }
            if (GameState.CurrentDialog.CurrentLine.SpeakerSource == DialogSource.BOSS)
                CameraEffects.Trauma = Mathf.Max(0.8f, CameraEffects.Trauma);
        }
    }
}
