using Godot;
using System;

public class EntityProgressBar : ProgressBar
{
    private EntityRep _entity;

    public override void _Ready()
    {
        base._Ready();

        _entity = this.FindParent<EntityRep>();
        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        var entity = _entity.GetEntity();
        if (entity != null)
        {
            MaxValue = entity.MaxHealth;
            Value = entity.Health;
        }
    }
}
