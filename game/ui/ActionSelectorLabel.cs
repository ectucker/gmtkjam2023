using Godot;
using System;

public class ActionSelectorLabel : Label
{
    public override void _Ready()
    {
        base._Ready();

        GameStateHolder.Instance.Connect(nameof(GameStateHolder.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameState.SelectedEntity?.Team == Team.PLAYER && GameState.SelectedEntity?.Health > 0)
        {
            Text = $"{GameState.SelectedEntity.Name}'s Actions";
            Visible = true;
        }
        else
        {
            Visible = false;
        }
    }
}
