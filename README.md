﻿# GMTk Jam 2023

This (will be) a game for the [Game Maker's Toolkit Game Jam 2023](https://itch.io/jam/gmtk-2023).
The game is developed using the Godot Game Engine.

The initial repo state was created from the template at [EctGodotUtils](https://github.com/ectucker1/EctGodotUtils/tree/3.x).
